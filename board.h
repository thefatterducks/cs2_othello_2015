#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;  
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    // Board(long int taken, long int black);
    ~Board();
    Board *copy();
    Board *goodCopy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    std::vector <Move *> getAllMoves(Side side);

    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    int scoreDifference(Side side);
    Move* getGoodMove(Side side);
    Move* getMinimaxGoodMove(Side side);
    void undoMove(Move *m);
    Side oppSide(Side side);

    void setBoard(char data[]);

};

#endif
