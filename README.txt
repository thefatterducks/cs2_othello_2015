Describe how and what each group member contributed for the past two weeks. If you are solo, these points will be part of the next section.

<<<<<<< HEAD
Kyle: I wrote the preliminary AI that was able to play the game, and sometimes beat 
      SimplePlayer by implementing a rudimentary heuristic. I also set up the bitbucket.
Algae: In said preliminary AI I made it care about corner and edge lcoations and 
       implemented a broken minimax. Implemented most of the Scheme code, with Kyle's help 
       in debugging, after OCaml didn't work out because it turns out learning a new language's 
       syntax and rules (especially OCaml's, the internet just did not want to give any help for 
       doing things in files to compile rather than the  interpreter)

Together we worked on determining what data types we would need in OCaml (BigInt vs Int vs Int64 
for the board state etc.) as well as learning enough OCaml syntax to implement the functions necessary 
for the player to work. 
And then Scheeeemmmmeeeee!


3 Document the improvements that your group made to your AI to make it tournament-worthy. Explain why you think your strategy(s) will work. Feel free to discuss any ideas were tried but didn't work out.

We decided to use OCaml for the beauty (fun) of functional programming with speed on similar levels 
as C++ when compiled and optimized. We checked that OCaml compilied code runs on the cluster 
computers. We implemented a basic heursitic in C++, and attempted Minimax for week 9. 

To then improve the OCaml code, we destroyed it and started using Scheme. If we define tournament worthy 
as "it was a whole lot of fun to write and a great way to procrastinate on CS4" then trivially this 
strategy would work. If we defne tournament worthy as "is capable of playing games with the other AIs" 
then it's much less trivial that the strategy would work. It did in the end because of bash hackery 
and rather gross code duplication (it turns out that as scheme isn't really a compiled language, it 
doesn't know what command line arguments are). It still might fail though, I never bothered to check 
if the particular way of compiling which we used would produce executables that run on the cluster 
computers. There's at least one way of doing it, I'm just not sure it's the way we did it
