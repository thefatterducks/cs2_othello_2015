#include "board.h"
#include <vector>
#include <iostream>
using namespace std;

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {

    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}
// Board::Board(long int taken, long int black){
//     this->taken = taken;
//     this->black = black;
// }

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

Side Board::oppSide(Side side){
    return (side == BLACK) ? BLACK : WHITE;
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    // cerr <<"hasMoves"<<endl;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}
vector<Move *> Board::getAllMoves(Side side) {
    vector<Move *> listOfMoves = vector<Move *>();
    // cerr<<"start get all moves"<<endl;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            // cerr<<"pre-move"<<endl;
            Move move (i, j);
            // cerr<<i<<":"<<j<<endl;
            if (checkMove(&move, side)){
                // cerr<<"got inside"<<endl;
                listOfMoves.push_back(new Move(i, j));
                //dies when the move is valid
            }
        }
    }
    // cerr<<"length"<<listOfMoves.size()<<endl;
    return listOfMoves;
}

// Finds a good next move for side based on number advantage, number of moves advantage,
// and move location.
Move* Board::getGoodMove(Side side){
    vector<Move *> listOfMoves = getAllMoves(side);
    int bestIndex = -1;
    int bestScore = -999; // too low
    int oppMoves = 60; //> possible
    
    for(int i = 0; i < (signed int) listOfMoves.size(); i++){
        Board * oldBoard = copy();
        oldBoard->doMove(listOfMoves[i], side);
        int propScore = oldBoard->scoreDifference(side);

        // Weight the proposed move's score according to location
        if (listOfMoves[i]->getX() == 1 or listOfMoves[i]->getX() == 6 or
                listOfMoves[i]->getY() == 1 or listOfMoves[i]->getY() == 6) // adjacent to edge, sad
            propScore -= 50;
        if (listOfMoves[i]->getX() == 0 or listOfMoves[i]->getX() == 7 or
                listOfMoves[i]->getY() == 0 or listOfMoves[i]->getY() == 7) // at edge, happy unless
            propScore *= 2;                                                 // adjacent to corner
        if ((listOfMoves[i]->getX() == 0 and (listOfMoves[i]->getY() == 0 or listOfMoves[i]->getY() == 7)) or 
                (listOfMoves[i]->getX() == 7 and (listOfMoves[i]->getY() == 0 or listOfMoves[i]->getY() == 7))) // at corner, happy
            propScore += 100;


        if (bestScore < propScore)
        {
            bestScore = propScore;
            bestIndex = i; 
            oppMoves = oldBoard->getAllMoves(oppSide(side)).size();
        }
        else if(bestScore == propScore)
        {
            // Choose the proposed move if it gives the opponent fewer options
            int propOppMoves = oldBoard->getAllMoves(oppSide(side)).size();
            if (propOppMoves < oppMoves)
            {
                bestScore = oldBoard->scoreDifference(side);
                bestIndex = i;   
                oppMoves = propOppMoves;
            }
        }
    }
    return listOfMoves[bestIndex];
}

Move* Board::getMinimaxGoodMove(Side side){
    vector<Move *> listOfMoves = getAllMoves(side);
    vector<int> moveScores;
  
    for (int i = 0; i < (signed int) listOfMoves.size(); i++){
        cerr << "1: (" << listOfMoves[i]->getX() << ", " << listOfMoves[i]->getY() << ")" << endl;
        // Make a copy of the board to try listOfMoves[i]
        Board * oldBoard = copy();
        oldBoard->doMove(listOfMoves[i], side);

        // Get available moves on the changed copy and iterate through trying them
        vector<Move *> subListOfMoves = oldBoard->getAllMoves(oppSide(side));
        int minScore = 999;
        for (int j = 0; j < (signed int) subListOfMoves.size(); j++){
            cerr << "2: (" << subListOfMoves[i]->getX() << ", " << subListOfMoves[j]->getY() << ")" << endl;
            // Make of copy of the changed copy to try subListOfMoves[j]
            Board * secondLevelBoard = oldBoard->copy();
            secondLevelBoard->doMove(subListOfMoves[j], oppSide(side));
            
            // See if this move gives a lesser score than the other options
            // just use the naive scoring
            minScore = min(minScore, secondLevelBoard->scoreDifference(side));
            cerr << "score is: " <<  secondLevelBoard->scoreDifference(side) << endl;
        }
        // record the min score from this move's available moves
        moveScores.push_back(minScore);
    }

    // Pick the move that gives the least min score
    int maxMin = -999;
    int maxMinInd = -1;
    for (int i = 0; i < (signed int) moveScores.size(); i++){
        if (maxMin < moveScores[i]){
            maxMin = moveScores[i];
            maxMinInd = i;
        }
    }
    return listOfMoves[maxMinInd];
}

//num moves aval / number of moves for opponent

void Board::undoMove(Move * move){
    //use bitset to undo a given move?
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // cerr<<"checkMove"<<endl;
    // cerr<<(*m).getX()<<":"<<(*m).getY()<<endl;
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)){
        cerr << "invalid move" << endl;
        return;
    }

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    // cerr<<"do move"<<endl;
    // cerr<<X<<":"<<Y<<endl;
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * score based on differnece in black and white stones
 */
 int Board::scoreDifference(Side side){
    //cerr<<"Black SCore" << board.countBlack() - board.countWhite() <<endl;
    if (side == BLACK){
        return countBlack() - countWhite();
    }
    else{
        return countWhite() - countBlack();
    }
 }

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
